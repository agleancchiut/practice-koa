import $ from 'jquery';

import styles from './entry.css';


if (module.hot) {
    module.hot.accept();
}

const cssModule = (arr, styles) => {
    let modulize = {};

    arr.forEach((id) => {
        let target = document.getElementById(id);

        if (target) {

            target.className += styles[id];

            let obj = {};
            obj[id] = target;

            Object.assign(modulize, obj);
        }
    });

    return modulize;
};
