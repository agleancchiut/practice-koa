const config = {
    lang: 'zh_TW',
    title: '匯彩科技股份有限公司',
    slogan: '匯印繽紛生活的色彩',
    description: '匯彩科技股份有限公司',
    googleAnalyticsId: 'UA-XXXXX-X',
};

export default config;
