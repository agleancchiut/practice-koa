import path from 'path';

import Koa from 'koa';
import serve from 'koa-static';
import template from 'koa-nunjucks-2';
import webpack from 'webpack';
import { devMiddleware, hotMiddleware } from 'koa-webpack-middleware';

import config from './config';
import webpackConfig from './webpack.config.js';

const app = new Koa();

app.use(async (ctx, next) => {
    ctx.state.config = config;
    await next();
});

app.use(serve('src/static'));

const compile = webpack(webpackConfig);

app.use(devMiddleware(compile, {

    // public path to bind the middleware to
    // use the same as in webpack
    publicPath: webpackConfig.output.publicPath,

    // options for formating the statistics
    stats: webpackConfig.stats,
}));

app.use(hotMiddleware(compile, {

}));

app.use(template({
    path: path.join(__dirname, 'src/templates'),
}));

app.use(async (ctx) => {
    await ctx.render('index', {msg: 'hello world!'});
});

app.listen(8000);
