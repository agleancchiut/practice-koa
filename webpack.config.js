import path from 'path';
import webpack from 'webpack';
import autoprefixer from 'autoprefixer';
import postcssImport from 'postcss-import';
import postcssURL from 'postcss-url';
import postcssCustomProperties from 'postcss-custom-properties';
import postcssNesting from 'postcss-nesting';
import postcssExtend from 'postcss-extend';
import postcssLost from 'lost';


const AUTOPREFIXER_BROWSERS = [
    'Android 2.3',
    'Android >= 4',
    'Chrome >= 35',
    'Firefox >= 31',
    'Explorer >= 9',
    'iOS >= 7',
    'Opera >= 12',
    'Safari >= 7.1',
];

const config = {
    entry: [
        'eventsource-polyfill',
        'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000',
        'babel-polyfill',
        path.resolve(__dirname, './src/client/entry.js'),
    ],
    output: {
        path: path.resolve(__dirname, './dist/static'),
        publicPath: '/public/',
        filename: 'app.bundle.js',
    },
    stats: {
        colors: true,
        reasons: true,
        hash: false,
        version: false,
        timings: true,
        chunks: false,
        chunkModules: false,
        cached: false,
        cachedAssets: false,
    },
    plugins: [
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin(),
    ],
    module: {
        loaders:[
            {
                test: /\.jsx?$/,
                include: path.resolve(__dirname, './src/client'),
                loaders: [
                    'imports?$=jquery,jQuery=jquery,this=>window',
                    'babel',
                ],
            },
            {
                test: /\.(png|jpg|jpeg|gif|svg|woff|woff2)$/,
                loader: 'url?limit=8192',
            }, {
                test: /\.(eot|otf|ttf|wav|mp3)$/,
                loader: 'file',
            },
            {
                test: /\.css$/,
                loaders: [
                    'style?sourceMap',
                    'css?modules&importLoaders=1&localIdentName=[path]___[name]__[local]___[hash:base64:5]',
                    'postcss',
                ],
            },
        ],
    },
    postcss: function (webpack) {
        return [
            postcssImport({
                addDependencyTo: webpack,
            }),
            postcssURL,
            postcssCustomProperties,
            postcssNesting,
            postcssExtend,
            postcssLost,
            autoprefixer({
                browsers: AUTOPREFIXER_BROWSERS,
            }),
        ];
    },
};

export default config;
