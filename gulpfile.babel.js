import path from 'path';

import gulp from 'gulp';
import gutil from 'gulp-util';
import nodemon from 'gulp-nodemon';
import webpack from 'webpack';
import webpackConfig from './webpack.config.js';


gulp.task('default', () => {
  // place code for your default task here
});

gulp.task('dev', () => {

    nodemon({
        script: 'app.js',
        ext: 'js njk',
        ignore: [
            path.resolve(__dirname, './src/client/'),
            path.resolve(__dirname, './node_modules/'),
        ],
        exec: path.resolve('./node_modules/.bin/babel-node'),
    })
    .on('start', () => {
        gutil.log('[express]', gutil.colors.magenta('server start'));
    })
    .on('restart', () => {
        gutil.log('[express]', gutil.colors.magenta('server restart'));
    });
});

gulp.task('webpack', (callback) => {
    webpack(webpackConfig, (err, stats) => {
        if (err) throw new gutil.PluginError('weboack', err);

        gutil.log('[webpack]', stats.toString(webpackConfig.stats));

        callback();
    });
});
